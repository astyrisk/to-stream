from innertube import InnerTube
import sys
import json
from pprint import pprint

PARAMS_TYPE_VIDEO = "EgIQAQ%3D%3D"
client = InnerTube("WEB")

if len(sys.argv) > 1:
    data = client.search(sys.argv[1], params=PARAMS_TYPE_VIDEO)
    items = data["contents"]["twoColumnSearchResultsRenderer"]["primaryContents"][ "sectionListRenderer" ]["contents"][0]["itemSectionRenderer"]["contents"][0:10]

    pprint(items[0]["videoRenderer"]["longBylineText"]['runs'][0]["text"])