from innertube import InnerTube
import sys
import json
from pprint import pprint

PARAMS_TYPE_VIDEO = "EgIQAQ%3D%3D"
client = InnerTube("WEB")

if len(sys.argv) > 1:
    data = client.search(sys.argv[1], params=PARAMS_TYPE_VIDEO)
    items = data["contents"]["twoColumnSearchResultsRenderer"]["primaryContents"][ "sectionListRenderer" ]["contents"][0]["itemSectionRenderer"]["contents"][0:10]
    selected_items = []
    for item in items:
        if "videoRenderer" not in item.keys():
            continue
        else:
            video = item["videoRenderer"]
            selected_item = {
                "videoId":  video["videoId"],
                "videoTitle": video["title"]["runs"][0]["text"],
                "videoThumbnail": video['thumbnail']["thumbnails"][-1]["url"],
                "videoChannel": video["longBylineText"]['runs'][0]["text"],
            }
            # selected_item = {
            #     "videoId": video_id,
            #     "videoTitle": video_title
            # }
            selected_items += [selected_item]

    print(json.dumps(selected_items))
