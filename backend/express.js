const express = require('express');
const cors = require('cors');
const { exec, spawn } = require('child_process');
const {get} = require("axios");
const app = express();
const port = 3001;

app.use(cors());

app.get('/search', (req, res) => { 
  const searchQuery = req.query.search;
  const python = spawn('./innertube/bin/python', ['./scripts/search_videos.py', searchQuery]);
  let dataToSend = '';

  python.stdout.on('data', (data) => {
    dataToSend += data.toString();
  });

  python.on('close', (code) => {
    console.log(dataToSend);
    console.log(`Python script exited with code ${code}`);
    res.send(dataToSend);
  });

});

app.get('/download/:videoId', (req, res) => {
 const videoId = req.params.videoId;
 const command = `yt-dlp -x --audio-format mp3 "https://www.yewtu.be/watch?v=${videoId}" -o "./downloads/${videoId}.mp3"`;

 exec(command, (error, stdout, stderr) => {
    if (error) {
      console.log(`error: ${error.message}`);
      return;
    }
    if (stderr) {
      console.log(`stderr: ${stderr}`);
      return;
    }
    console.log(`stdout: ${stdout}`);
    res.sendFile(`./downloads/${videoId}.mp3`, { root: __dirname });
 });
});

app.get('/stream/:videoId', async (req, res) => {
    const videoId = req.params.videoId;
    console.log(videoId);
    try {
        const response = await get(`https://pipedapi.kavin.rocks/streams/${videoId}`);
        let data = response.data["audioStreams"][0]["url"];
        console.log(data);
        res.send(data);
        console.log(data);
        console.log("----------------------------------------------------------------------------------");
    } catch (error) {
        console.log(error);
    }


    // const ytDlp = spawn('yt-dlp', ['-x', '--audio-format', 'mp3', "https://www.youtube.com/watch?v=" + videoId, '-o', '-']);
    //
    // ytDlp.stdout.on('data', (data) => {
    //     res.write(data);
    // });
    //
    // ytDlp.stderr.on('data', (data) => {
    //     console.error(`stderr: ${data}`);
    // });
    //
    // ytDlp.on('close', (code) => {
    //     res.end();
    //     console.log(`child process exited with code ${code}`);
    // });

});


app.listen(port, () => {
 console.log(`Server running at http://localhost:${port}`);
});
