import { writable, get } from 'svelte/store';

export const audioPlayer = writable();
export const currentTrack = writable();
export const status = writable('default');
export const isPlaying = writable(false);
export const index = writable(0);

export const trackList = writable([
    {
        title: 'Requiem in D minor, K. 626',
        artist: 'Wolfgang Amadeus Mozart',
        file: 'https://sveltejs.github.io/assets/music/mozart.mp3',
        thumbnail: 'https://i.ytimg.com/vi/siO6dkqidc4/hq720.jpg?sqp=-oaymwE2CNAFEJQDSFXyq4qpAygIARUAAIhCGAFwAcABBvABAfgB_gmAAtAFigIMCAAQARhlIGUoZTAP&rs=AOn4CLD_T5-fM3cjDgAnrywKJkw7YfvU8A"',
    },
    {
        title: 'Symphony no. 5 in Cm, Op. 67',
        artist: 'Ludwig van Beethoven',
        file: 'https://sveltejs.github.io/assets/music/beethoven.mp3',
        thumbnail: 'https://i.ytimg.com/vi/siO6dkqidc4/hq720.jpg?sqp=-oaymwE2CNAFEJQDSFXyq4qpAygIARUAAIhCGAFwAcABBvABAfgB_gmAAtAFigIMCAAQARhlIGUoZTAP&rs=AOn4CLD_T5-fM3cjDgAnrywKJkw7YfvU8A"',
    }
]);

export const addTrack = track => {
    trackList.update(v => [...v, track])
};
